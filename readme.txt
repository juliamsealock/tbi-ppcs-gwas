11-05-2021
Code for pulling phenotypes for TBI and PPCS

To clone repo: git clone https://juliamsealock@bitbucket.org/juliamsealock/tbi-ppcs-gwas.git

Using script:
tbi_ppcs_phenotype_pull_functions.r file will be sourced in an R session to load functions

tbi_ppcs_phenotype_pull.r contains code to pull the phenotypes. this file will need to be customized with paths for icd codes, cpt codes, and principal components files

tbi_icd_codes.txt contains list of ICD codes to define TBI and mTBI

Files needed:
- ICD codes
- CPT codes
- demographics containing date of birth
- principal components for ancestries
