### code uses functions.r code to pull TBI and PPCS phenotypes

source("tbi_ppcs_phenotype_pull_functions.r")

## specify file paths
icd.path <- "/path/to/icd/code/file" ## ICD codes columns: ID, ICD_CODE, ICD_DATE, and ICD_VOCAB
cpt.path <- "/path/to/cpt/code/file" ## CPT codes columns: ID, CPT_CODE, and CPT_DAT
demo.path <- "/path/to/demo/code/file" ## demo columns: GRID, DOB

euro_geno_pcs_path <- "/path/to/pc1-10/euro_ancestry" ## PC files columns: ID, PC1-PC10
afr_geno_pcs_path <- "/path/to/pc1-10/afr_ancestry"

## load data 
tbi_codes <- read.table("tbi_icd_codes.txt", header=T, sep="\t")
icd <- fread(paste0(icd.path), header=T) 
cpt <- fread(paste0(cpt.path), header=T) E
demo <- fread(paste0(demo.path), header=T) 
dob <- demo[, c("ID","DOB")]

euro_geno_pcs <- read.table(paste0(euro_geno_pcs_path), header=T, sep="\t") 
afr_geno_pcs <- read.table(paste0(afr_geno_pcs_path), header=T, sep="\t")

## subset icd file to IDs, dates, codes, and vocab
## subset cpt file to IDs, dates, and codes
### this just makes the code easier to use across sites with varying naming conventions
icd2 <- icd[,c("ID","ICD_DATE","ICD_CODE","ICD_VOCAB")]
cpt2 <- cpt[,c("ID","CPT_DATE","CPT_CODE")]
colnames(icd2) <- c("ID","DATE","CODE","VOCAB")
colnames(cpt2) <- c("ID","DATE","CODE")

#################
###### TBI ######
#################

## find TBI status

tbi_status_euro <- get_tbi_status(icd=icd2, tbi_codes=tbi_codes, pcs=euro_geno_pcs)
tbi_status_afr <- get_tbi_status(icd=icd2, tbi_codes=tbi_codes, pcs=afr_geno_pcs)

write.table(tbi_status_euro, "tbi_any_severity_euro_phenotype_file_all.txt", col.names=T, row.names=F, sep="\t", quote=F)
write.table(tbi_status_afr, "tbi_any_severity_afr_phenotype_file_all.txt", col.names=T, row.names=F, sep="\t", quote=F)

## find mTBI status

mild_tbi_status_euro <- get_mild_tbi_status(tbi_codes=tbi_codes, icd=icd, tbi_status=tbi_status, pcs=euro_geno_pcs)
mild_tbi_status_afr <- get_mild_tbi_status(tbi_codes=tbi_codes, icd=icd, tbi_status=tbi_status, pcs=afr_geno_pcs)

write.table(mild_tbi_status_euro, "mild_tbi_euro_phenotype_file_all.txt", col.names=T, row.names=F, quote=F, sep="\t")
write.table(mild_tbi_status_afr, "mild_tbi_afr_phenotype_file_all.txt", col.names=T, row.names=F, sep="\t", quote=F)


##################
###### PPCS ######
##################

## find people with exclusion codes
excluded_ids <- find_exclusions(tbi_codes=tbi_codes, cpt=cpt2, icd=icd2)

## find PPCS status

ppcs_status_euro <- find_ppcs_status(icd=icd2, exclusion_ids=excluded_ids, dob=dob, code_time_diff="14", age_at_last_code="5", princ_comp=euro_geno_pcs)
ppcs_status_afr <- find_ppcs_status(icd=icd2, exclusion_ids=excluded_ids, dob=dob, code_time_diff="14", age_at_last_code="5", princ_comp=afr_geno_pcs)

write.table(ppcs_status_euro, "ppcs_euro_phenotype_file_all.txt", col.names=T, row.names=F, quote=F, sep="\t")
write.table(ppcs_status_afr, "ppcs_afr_phenotype_file_all.txt", col.names=T, row.names=F, quote=F, sep="\t")