## function for pulling TBI and PPCS phenotypes

library(data.table)
library(dplyr)

## find tbi status
get_tbi_status <- function(icd=icd, tbi_codes=tbi_codes, pcs=pcs){
    tbi_cases <- icd[(icd$CODE %in% tbi_codes$ICD),]
    tbi_controls <- icd[!(icd$ID %in% tbi_cases$ID),]
    tbi_cases_dedup <- tbi_cases[!duplicated(tbi_cases$ID),]
    tbi_controls_dedup <- tbi_controls[!duplicated(tbi_controls$ID),]

    tbi_cases_dedup$TBI <- "TRUE"
    tbi_controls_dedup$TBI <- "FALSE"
    tbi_status <- rbind(tbi_cases_dedup, tbi_controls_dedup)
    tbi_status2 <- tbi_status[, c("ID","TBI")]
    tbi_status_pcs <- merge(tbi_status2, pcs, by="ID")
    return(tbi_status_pcs)
}

## find mtbi 

get_mild_tbi_status <- function(tbi_codes=tbi_codes, icd=icd, tbi_status=tbi_status, pcs=pcs){
    mild <- subset(tbi_codes, Severity.Classification=="Mild" | Severity.Classification=="Mild " | Severity.Classification==" Mild")
    severe <- subset(tbi_codes, Severity.Classification=="Moderate" | Severity.Classification=="Penetrating" | Severity.Classification=="Severe" | Severity.Classification=="Severe ")

    mild_cases <- icd[(icd$CODE %in% mild$ICD),]
    severe_cases <- icd[(icd$CODE %in% severe$ICD),]
    mild_cases_no_severe <- mild_cases[!(mild_cases$ID %in% severe_cases$ID),]

    mild_cases_no_severe_dedup <- mild_cases_no_severe[!duplicated(mild_cases_no_severe$ID),]
    mild_cases_no_severe_dedup$mild_tbi <- "TRUE"

    tbi_controls <- subset(tbi_status, TBI=="FALSE")
    tbi_controls$mild_tbi <- "FALSE"

    mild_tbi_status <- rbind(mild_cases_no_severe_dedup, tbi_controls)
    mild_tbi_status2 <- mild_tbi_status[, c("ID","mild_tbi")]
    mild_tbi_status_pcs <- merge(mild_tbi_status2, pcs, by="ID")
    return(mild_tbi_status_pcs)
}


## find neurosurgery exclusions
find_exclusions <- function(tbi_codes=tbi_codes, cpt=cpt, icd=icd){
    #### moderate/severe tbi codes
    mod_severe_tbi_codes <- subset(tbi_codes, Severity.Classification=="Moderate" | Severity.Classification=="Penetrating" | Severity.Classification=="Severe" | Severity.Classification=="Severe ")
    mod_severe_tbi_icd <- icd[(icd$CODE %in% mod_severe_tbi_codes$ICD),]

    #### neurosurg procedures
    neurosurg_cpt <- subset(cpt, CODE>=61000 & CODE<=62258)

    #### icd exclusion codes
    icd.procedures1 <- c("01.", "02.")
    icd.procedures2 <- c("00", "0N")
    icd.procedures3 <- c("R40.242", "R40.243")
    neurosurg_icd1 <- subset(icd, (substr(CODE, 1, 3) %in% icd.procedures1 & VOCAB=="ICD9Proc") | (substr(CODE, 1, 2) %in% icd.procedures2 & VOCAB=="ICD10PCS") | (substr(CODE, 1, 7) %in% icd.procedures3 & VOCAB=="ICD10CM")) 

    ## combind icd, ctp, mod/sev tbi exclusions
    neurosurg_cpt$VOCAB <- "CPT"
    excl_codes <- rbind(neurosurg_icd1, neurosurg_cpt, mod_severe_tbi_icd)
    excl_codes_unique_ids <- excl_codes[!duplicated(excl_codes$ID),]
    return(excl_codes_unique_ids)
}


## get ppcs status
find_ppcs_status <- function(icd=icd, exclusion_ids=exclusion_ids, dob=dob, code_time_diff=code_time_diff, age_at_last_code=age_at_last_code, princ_comp=princ_comp){

    pcs <- subset(icd, CODE=="310.2" | CODE=="F07.81")

    ## get cases 
        ## filter for at least 2 pcs cods
        pcs_code_count <- pcs %>% group_by(ID) %>% count(ID)
        pcs_code_count2 <- subset(pcs_code_count, n>=2)
        pcs_2codes <- pcs[(pcs$ID %in% pcs_code_count2$ID),]

        ## require at least 14 days between first and last pcs code
        first_pcs_code <- setDT(pcs_2codes)[order(DATE), head(.SD,1L),by="ID"]
        last_pcs_code <- setDT(pcs_2codes)[order(-DATE), head(.SD,1L),by="ID"]
        first_pcs_code2 <- first_pcs_code[, c("ID","DATE")]
        last_pcs_code2 <- last_pcs_code[, c("ID","DATE")]
        colnames(first_pcs_code2)[2] <- "first_pcs_code"
        colnames(last_pcs_code2)[2] <- "last_pcs_code"
        first_last_pcs_codes <- merge(first_pcs_code2, last_pcs_code2)
        first_last_pcs_codes$time_diff <- as.numeric(as.Date(first_last_pcs_codes$last_pcs_code, "%Y-%m-%d") - as.Date(first_last_pcs_codes$first_pcs_code, "%Y-%m-%d"))
        pcs_time_censor <- subset(first_last_pcs_codes, time_diff>as.numeric(code_time_diff))

        ## >=5 years old at last pcs code
        pcs_time_censor_demo <- merge(pcs_time_censor, dob, by="ID")
        pcs_time_censor_demo$age_at_last_pcs_code <- as.numeric(as.Date(pcs_time_censor_demo$last_pcs_code, "%Y-%m-%d")- as.Date(pcs_time_censor_demo$DOB, "%Y-%m-%d"))/365.25
        pcs_time_censor_demo_age_censor <- subset(pcs_time_censor_demo, age_at_last_pcs_code>=as.numeric(age_at_last_code))

        ## remove individuals with moderate/severe tbi or neurosurg procedures 
        pcs_time_censor_demo_age_censor_no_excl <- pcs_time_censor_demo_age_censor[!(pcs_time_censor_demo_age_censor$ID %in% exclusion_ids$ID),]

        ## get cases
        pcs_time_censor_demo_age_censor_no_excl$PPCS <- "TRUE"
        ppcs_cases <- pcs_time_censor_demo_age_censor_no_excl[,c("ID","PPCS")]

    ## get controls
        mild_tbi_codes <- subset(tbi_codes, Severity.Classification=="Mild" | Severity.Classification=="Mild ")
        mild_tbi_icd <- icd[(icd$CODE %in% mild_tbi_codes$ICD),]

        ## at least 5yo at last mtbi code
        last_mtbi_icd <- setDT(mild_tbi_icd)[order(-DATE), head(.SD,1L),by="ID"]
        last_mtbi_icd_demo <- merge(last_mtbi_icd, dob, by="ID")
        last_mtbi_icd_demo$age_at_last_mtbi_code <- as.numeric(as.Date(last_mtbi_icd_demo$DATE, "%Y-%m-%d") - as.Date(last_mtbi_icd_demo$DOB, "%Y-%m-%d"))/365.25
        last_mtbi_icd_demo_age_censor <- subset(last_mtbi_icd_demo, age_at_last_mtbi_code>=as.numeric(age_at_last_code))

        ## remove individuals with neurosurg codes or mod/severe tbi
        last_mtbi_icd_demo_age_censor_no_excl <- last_mtbi_icd_demo_age_censor[!(last_mtbi_icd_demo_age_censor$ID %in% exclusion_ids$ID),]

        ## remove individudals with pcs code
        last_mtbi_icd_demo_age_censor_no_excl_no_pcs <- last_mtbi_icd_demo_age_censor_no_excl[!(last_mtbi_icd_demo_age_censor_no_excl$ID %in% pcs$ID),]

        ## get controls
        last_mtbi_icd_demo_age_censor_no_excl_no_pcs$PPCS <- "FALSE"
        ppcs_controls <- last_mtbi_icd_demo_age_censor_no_excl_no_pcs[,c("ID","PPCS")]

    ## combine cases and controls and add pcs
    ppcs_status <- rbind(ppcs_cases, ppcs_controls)
    ppcs_status_pcs <- merge(ppcs_status, princ_comp, by="ID")
    return(ppcs_status_pcs)
}
